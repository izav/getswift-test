﻿﻿# **[GetSwift Code Test](https://github.com/GetSwift/codetest)**
 -----------------------------------

Project: **[http://tst20128-001-site1.atempurl.com/getswift/test](http://tst20128-001-site1.atempurl.com/getswift/test)** 

Software Used: C#, ASP.NET MVC, Bootstrap

-----------------------------------------

## The scenario
You run a drone delivery company. Packages arrive at your depo to be delivered by your fleet of drones. 

## The problem
Your solution should take two inputs: a list of drones, and a list of packages; and produce two outputs: a list of assignments of packages to drones, and a list of packages that could not be assigned. For example, you could produce the following output:

```javascript
{
  assignments: [{droneId: 1593, packageId: 1029438}, {droneId: 1251, packageId: 1029439}]
  unassignedPackageIds: [109533, 109350, 109353]
}
```

### Constraints
There's a number of constraints that your solution must obey:

- The depo is located at 303 Collins Street, Melbourne, VIC 3000
- Drones might already be carrying a package. The time to deliver this package should be taken into account when comparing drones.
- Once a drone is assigned a package, it will fly in a straight line to its current destination (if it already has a package), then to the depo, then to the new destination
- Packages must only be assigned to a drone that can complete the delivery by the package's delivery deadline
- Packages should be assigned to the drone that can deliver it soonest
- A drone should only appear in the assignment list at most once; this is a dispatching problem, not a routing problem. For example, this is allowed:
```javascript
{
  assignments: [{droneId: 1593, packageId: 1029438}, {droneId: 1251, packageId: 1029439}]
  unassignedPackageIds: [109533, 109350, 109353]
}
```
but this is not allowed:
```javascript
{
  assignments: [{droneId: 1593, packageId: 1029438}, {droneId: 1593, packageId: 1029439}]
  unassignedPackageIds: [109533, 109350, 109353]
}
```

### Assumptions
You can make the following simplifying assumptions:

- Drones have unlimited range
- Drones travel at a fixed speed of 50km/h
- Packages are all the same weight and volume
- Packages can be delivered early
- Drones can only carry one item at a time

You should integrate with [this API](https://codetest.kube.getswift.co/drones) which generates randomized data.

You can use any language and/or framework to solve this. Please also give an indication of how you envisage your solution will be deployed, and what other components it might interact with.

## The API
The API lives at https://codetest.kube.getswift.co/drones. You'll need to use two methods:

### `GET /drones`
This returns a randomized list of drones. A drone can have up to one package assigned to it.

```javascript
[
    {
        "droneId": 321361,
        "location": {
            "latitude": -37.78290448241537,
            "longitude": 144.85335277520906
        },
        "packages": [
            {
                "destination": {
                    "latitude": -37.78389758422243,
                    "longitude": 144.8574574322506
                },
                "packageId": 7645,
                "deadline": 1500422916
            }
        ]
    },
    {
        "droneId": 493959,
        "location": {
            "latitude": -37.77718638788778,
            "longitude": 144.8603578487479
        },
        "packages": []
    }
]
```

### `GET /packages`
This returns a randomized list of packages. You will need to assign the packages from this endpoint to the drones returned from the other endpoint. The deadline here is a Unix timestamp.

```javascript
[
    {
        "destination": {
            "latitude": -37.78404125474984,
            "longitude": 144.85238118232522
        },
        "packageId": 8041,
        "deadline": 1500425202
    },
    {
        "destination": {
            "latitude": -37.77058198385452,
            "longitude": 144.85157121265505
        },
        "packageId": 8218,
        "deadline": 1500423287
    }
]
```


-----------------------------------

# **Screenshots**

Due limited hosting space GetSwift was hosted together with the tests:
[Tiny URL](https://bitbucket.org/irinazav/tiny-url-test/src)
and 
[Messages DB & Golden Pond](https://bitbucket.org/irinazav/test-messagesdb-and-goldenpond/src)



---------------------------------

All Tests - **[mytest2017-001-site1.ftempurl.com](http://mytest2017-001-site1.ftempurl.com)**

![Tests1.jpg](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/Tests1.jpg)

--------------------------------

GetSwift Test - **[mytest2017-001-site1.ftempurl.com/GetSwift/Test](http://mytest2017-001-site1.ftempurl.com/getswift/test)**

![GetSwiftTest.png](https://bitbucket.org/irinazav/take-home-interview-tests/downloads/GetSwift1.png)













