﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.Models.MessagesDB
{
    public class ParticipantType
    {
        public int Id { get; set; }
        public string Type { get; set; }

        [InverseProperty("Participant")]
        public virtual IEnumerable<Participant> Partisipants { get; set; }
    }
}