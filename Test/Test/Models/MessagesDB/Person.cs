﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.Models.MessagesDB
{
    public class Person
    {

        public int Id { get; set;}
        public string Name { get; set; }

        [InverseProperty("EmailAddress")]
        public virtual IEnumerable<EmailAddress> EmailAddresses { get; set; }
    }
}