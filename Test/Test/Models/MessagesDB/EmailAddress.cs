﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.Models.MessagesDB
{
    public class EmailAddress
    {
        public int Id { get; set; }
        
        public string AddressText { get; set; }

        public int DomainId { get; set; }
        public Domain Domain { get; set; }

        public int PersonId { get; set; }
        public Person Person { get; set; }

        [InverseProperty("Participant")]
        public virtual IEnumerable<Participant> Partisipants { get; set; }
    }
}