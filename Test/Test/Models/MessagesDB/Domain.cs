﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.Models.MessagesDB
{
    public class Domain
    {
        public int Id { get; set; }
        public string UrlText { get; set; }
       
        [InverseProperty("EmailAddress")]
        public virtual IEnumerable<EmailAddress> EmailAddresses { get; set; }
    }
}