﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.Models.MessagesDB
{
    public class Message
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string MessageText { get; set; }

        [InverseProperty("Participant")]
        public virtual IEnumerable<Participant> Partisipants { get; set; }
    }
}