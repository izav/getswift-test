﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Test.Models.MessagesDB
{
    public class Participant
    {
        public int Id { get; set; }

        public int ParticipantTypeId { get; set; }
        public ParticipantType ParticipantType { get; set; }

        public int EmailAddressId { get; set; }
        public EmailAddress EmailAddress { get; set; }
      
        public int MessageId { get; set; }
        public Message Message { get; set; }
        
    }
}