﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Models.GetSwiftTest
{
    public class Package
    {
        public int packageId {get; set;}
        public long deadline { get; set; }
        public Coordinate destination { get; set; }
    }
}