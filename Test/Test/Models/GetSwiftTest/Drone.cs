﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Test.Models.GetSwiftTest
{
    public class Drone
    {
        public int droneId { get; set; }
        public Coordinate location { get; set; }
        public IEnumerable<Package> packages { get; set; }
        
    }
}