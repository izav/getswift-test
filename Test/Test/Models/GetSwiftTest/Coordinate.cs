﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Test.Models.GetSwiftTest
{
    public class Coordinate
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}