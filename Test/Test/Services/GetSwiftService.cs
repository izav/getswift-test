﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Test.Models.GetSwiftTest;
using System.Web.Script.Serialization;
using System.IO;


namespace Test.Services
{
    public class GetSwiftService
    {
        readonly string uriDrones = "https://codetest.kube.getswift.co/drones";
        readonly string uriPackages = "https://codetest.kube.getswift.co/packages";
        

        public IEnumerable<Drone> GetDrones()
        {
            using (WebClient webClient = new WebClient())
            {
                return JsonConvert.DeserializeObject<IEnumerable<Drone>>(
                    webClient.DownloadString(uriDrones)
                );
            }
        }


        public async Task<IEnumerable<Drone>> GetDronesAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                return JsonConvert.DeserializeObject<IEnumerable<Drone>>(
                    await httpClient.GetStringAsync(uriPackages)
                );
            }
        }

        public IEnumerable<Package> GetPackages()
        {
            using (WebClient webClient = new WebClient())
            {
                return JsonConvert.DeserializeObject<IEnumerable<Package>>(
                    webClient.DownloadString(uriPackages)
                );
            }
        }


        public async Task<IEnumerable<Package>> GetPackagesAsync()
        {
            using (HttpClient httpClient = new HttpClient())
            {
                return JsonConvert.DeserializeObject<IEnumerable<Package>>(
                    await httpClient.GetStringAsync(uriPackages)
                );
            }
        }

        private static string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content/", "");
        public IEnumerable<Drone> GetDronesLocal()
        {
            string jsonInput = File.ReadAllText(filePath + "drones.json");
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<IEnumerable<Drone>>(jsonInput);
        }

        public IEnumerable<Package> GetPackagesLocal()
        {
            string jsonInput = File.ReadAllText(filePath + "packages.json");
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Deserialize<IEnumerable<Package>>(jsonInput);
        }


    }   
}