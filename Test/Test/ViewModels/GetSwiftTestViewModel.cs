﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models.GetSwiftTest
;
namespace Test.ViewModels
{
    public class GetSwiftTestViewModel
    {
        public IEnumerable<Drone> Drones { get; set; }
        public IEnumerable<Package> Packages { get; set; }

        public IEnumerable<int> RejectedPackages { get; set; }
        public IEnumerable<int> NotAssignedPackages { get; set; }
        public IEnumerable<AssignedPair> AssignedPackages { get; set; }
        
    }

    public class AssignedPair
    {
        public int droneId { get; set; }
        public int packageId { get; set; }
        public string delivery { get; set; }
        public string deadline { get; set; }
        public string needTime { get; set; }
    }
}