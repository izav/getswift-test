﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models.MessagesDB;

namespace Test.ViewModels
{
    public class MessagesDBViewModel
    {
        public string Title { get; set; }
        public IEnumerable<Domain> Domains { get; set; }
        public IEnumerable<EmailWithPersonName> EmailAddressesWithPersonNames { get; set; }
        public IEnumerable<DomainWithTotal> EmailsCountForDomains { get; set; }
    }

    public class EmailWithPersonName
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }

    public class DomainWithTotal
    {
        public int Id { get; set; }
        public string UrlText { get; set; }
        public int Count { get; set; }
    }
}