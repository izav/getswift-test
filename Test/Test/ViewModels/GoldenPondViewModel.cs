﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models.GoldenPond;

namespace Test.ViewModels
{
    public class GoldenPondViewModel
    {
        public DuckState StartState { get; set; }
        public DuckState EndState { get; set; }
        public string Commands { get; set; }
    }
}