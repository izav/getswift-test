﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models.MessagesDB;
using Test.ViewModels;

namespace Test.Repositories.MessagesDB
{
    public class MessagesDBRepsository : IMessagesDBRepository<Person, int>
    {
        private DbContextEF _context;

        public MessagesDBRepsository(DbContextEF context)
        {
            _context = context;
        }


        // SQL Queries ---------------------------
        public IEnumerable<Domain> GetDomainsSQL()
        {
                
             return _context.Database.SqlQuery<Domain>("sp_GetDomains");                        
        }

        public IEnumerable<EmailWithPersonName> GetEmailAddressesWithPersonNamesSQL()
        {

            return _context.Database.SqlQuery<EmailWithPersonName>("sp_GetEmailAddressesWithPersonNames");

        }

        public IEnumerable<DomainWithTotal> GetEmailsCountForDomainsSQL()
        {
            return _context.Database.SqlQuery<DomainWithTotal>("sp_GetEmailsCountForDomains");
        }




        //EF Queries ---------------------
        public IEnumerable<Domain> GetDomains()
        {
            var t = GetEmailsCountForDomainsSQL().ToList();
            return _context.Domains;
        }

        public IEnumerable<EmailWithPersonName> GetEmailAddressesWithPersonNames()
        {
            var result = _context.EmailAddresses
                           .Include("Person")
                           .OrderBy(k => k.AddressText)
                           .ThenBy(k => k.Person.Name)
                           .Select(k => new EmailWithPersonName { Email = k.AddressText, Name = k.Person.Name });
           return result;
                  
        }
        public IEnumerable<DomainWithTotal> GetEmailsCountForDomains()
        {
            var fromId = _context.PartisipantTypes
                           .Where( c=> c.Type.ToLower() == "from")
                           .FirstOrDefault().Id;

            var result = _context
                           .Participantes
                           .Where(p => p.ParticipantTypeId != fromId)
                           .Join(
                               _context.EmailAddresses,
                               i1 => i1.EmailAddressId,
                               i2 => i2.Id,
                               (i1, i2) => new { Partisicipant = i1, DomainId = i2.DomainId }
                            )
                            .GroupBy(k => k.DomainId)
                            .Select(k => new { DomainId = k.Key, Count = k.Count() })
                            .Join(
                               _context.Domains,
                               i1 => i1.DomainId,
                               i2 => i2.Id,
                               (i1, i2) => new { UrlText = i2.UrlText, Count = i1.Count }
                            )
                            .OrderBy(k => k.UrlText)
                            .AsEnumerable()
                            .Select(k => new DomainWithTotal { UrlText = k.UrlText, Count = k.Count });
                             
            return result;

        }
    }
}