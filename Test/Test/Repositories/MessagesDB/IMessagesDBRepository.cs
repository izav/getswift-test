﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models.MessagesDB;
using Test.ViewModels;

namespace Test.Repositories.MessagesDB
{
    public interface IMessagesDBRepository <T, TId> where T : class  
    {
        IEnumerable<Domain> GetDomains();
        IEnumerable<EmailWithPersonName> GetEmailAddressesWithPersonNames();
        IEnumerable<DomainWithTotal> GetEmailsCountForDomains();

        IEnumerable<Domain> GetDomainsSQL();
        IEnumerable<EmailWithPersonName> GetEmailAddressesWithPersonNamesSQL();
        IEnumerable<DomainWithTotal> GetEmailsCountForDomainsSQL();
    }
}