﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models.TinyUrl;



namespace Test.Repositories.TinyUrl
{
    public interface  ITinyUrlRepository <T, TId> where T : class      
    {
        IEnumerable<UrlMap> GetUrlMaps(int pageNo, int pageSize);
        int GetUrlMapsCount();
        UrlMap GetUrlMapById(int id);
        UrlMap GetUrlMapByTinyUrl(string tinyUrl);
        int GetNextId();
        UrlMap AddUrlMap(UrlMap urlMap);
        int DeleteUrlMap(int id);      
    }
}