﻿using Microsoft.Owin;
using Owin;
using Hangfire;
using Hangfire.Dashboard;
using Test.Repositories;
using Test.Controllers;


[assembly: OwinStartupAttribute(typeof(Test.Startup))]
namespace Test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            using (var context = new DbContextEF())
            {
                context.Database.Initialize(false);
            }
        }
    }
}
