﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Test.Models.GetSwiftTest;
using System.Device.Location;

namespace Test.Infrastructure
{
    public static class TimeHelper
    {
        //Depo: 303 Collins Street, Melbourne, VIC 3000
        public static readonly GeoCoordinate GeoDepoLocation = new GeoCoordinate(-37.816664, 144.963848);
  
        public static readonly DateTime EPOCH_UNIX = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public const double SPEED_M_SEC =  50000/3600;
        

        public static long TimeSpanCalculator(this Coordinate coordinate, Package package = null)
        {
            var geo1 = new GeoCoordinate(coordinate.latitude, coordinate.longitude);
            long result;
            if (package == null)
                result = Convert.ToInt64((geo1.GetDistanceTo(GeoDepoLocation) / SPEED_M_SEC));
            else
            {                
                var geo2 = new GeoCoordinate(package.destination.latitude, package.destination.longitude);       
                result = Convert.ToInt64((geo1.GetDistanceTo(geo2)  + geo2.GetDistanceTo(GeoDepoLocation))/ SPEED_M_SEC);
            }
           
            return result ;
        }


        public static string SecondsToString(this long seconds)
        {
            var timeSpan = TimeSpan.FromSeconds(seconds);
            int hr = timeSpan.Hours;
            int mn = timeSpan.Minutes;
            int sec = timeSpan.Seconds;
            var str = "Hr:" + hr + " Min:" + mn + " Sec:" + sec;
            return str;
        }


        public static string UnixDateToString(this long seconds)
        {
            var timeSpan = TimeSpan.FromSeconds(seconds);
            var dateTime = EPOCH_UNIX.Add(timeSpan);
            var str = String.Format("{0:MM/dd/yyyy HH:mm:ss}", dateTime);
            return str;
        }


        public static long UnixNow() 
        {
            return (long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
        }
        
    }
}