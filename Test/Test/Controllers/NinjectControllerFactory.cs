﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ninject;
using System.Configuration;
using System.Web.Routing;
using System.Web.Mvc;
using Test.Repositories.MessagesDB;
using Test.Repositories.TinyUrl;
using Test.Models.MessagesDB;
using Test.Models.TinyUrl;

namespace Test.Controllers
{
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel kernel;

        public NinjectControllerFactory()
        {
            kernel = new StandardKernel();
            AddBindingsDB();
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            return controllerType == null ? null : (IController)kernel.Get(controllerType);
        }


        private void AddBindingsDB()
        {
            //MessagesDB Test
            kernel.Bind<IMessagesDBRepository<Person, int>>().To<MessagesDBRepsository>();

            //TinyURL Test
            kernel.Bind<ITinyUrlRepository<UrlMap, int>>().To<TinyUrlRepository>();
        }
    }
}