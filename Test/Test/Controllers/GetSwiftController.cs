﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Device.Location;
using Test.Models.GetSwiftTest;
using Test.ViewModels;
using Test.Services;
using Test.Infrastructure;

namespace Test.Controllers
{
    public class GetSwiftController : Controller
    {
        public ActionResult Test() 
        {
            var service = new GetSwiftService();
            IEnumerable<Drone> dronesInput = service.GetDrones();
            IEnumerable<Package> packagesInput = service.GetPackages();

            var drones = dronesInput.Select(p => new
                {
                    droneId = p.droneId,
                    delayTimeSpan = p.location.TimeSpanCalculator(p.packages.FirstOrDefault())
                }) 
                .OrderBy(k => k.delayTimeSpan)
                .ToList();

            var nowUnixSeconds = TimeHelper.UnixNow();

            var packages = packagesInput
                .Select(p => new 
                {
                   packageId = p.packageId,
                   minTimeSpan = p.destination.TimeSpanCalculator(),
                   maxTimeSpan = p.deadline - nowUnixSeconds,                  
                });

           //wrong deadline, not able to delivery on time
            var rejectedPackages = packages
                .Where(p => p.maxTimeSpan < p.minTimeSpan)
                .Select(k => k.packageId)
                .OrderBy(k => k)
                .ToList();

            //Sorted by min gap between left time and calculated delivery time
            var approvedPackages = packages
                .Where(p => !rejectedPackages.Any(pp => pp == p.packageId))
                .OrderBy(k => k.maxTimeSpan - k.minTimeSpan)
                .ToList();

            List<AssignedPair> assignedPackages = new List<AssignedPair>();
            List<int> notAssignedPackages = new List<int>();

            foreach (var p in approvedPackages)
            {
                if (drones.Count() == 0) {
                    List<int> lastPackages = approvedPackages
                         .SkipWhile(k => k.packageId == p.packageId)
                         .Select(k => k.packageId)
                         .ToList();
                    notAssignedPackages.AddRange(lastPackages);
                    break;
                }

                var drone = drones.FirstOrDefault(k => p.maxTimeSpan >= k.delayTimeSpan + p.minTimeSpan);
                if (drone == null)
                {
                    notAssignedPackages.Add(p.packageId);
                }
                else 
                {
                    assignedPackages.Add(new AssignedPair()
                    {
                        droneId = drone.droneId, 
                        packageId = p.packageId,
                        delivery = (nowUnixSeconds + p.minTimeSpan + drone.delayTimeSpan).UnixDateToString(),
                        deadline = (nowUnixSeconds + p.maxTimeSpan).UnixDateToString(),
                        needTime = (drone.delayTimeSpan + p.minTimeSpan).SecondsToString()
                    });

                    drones.Remove(drone);                   
                }           
            }

            var vm = new GetSwiftTestViewModel(){
                Drones = dronesInput,
                Packages = packagesInput,
                AssignedPackages = assignedPackages,
                RejectedPackages = rejectedPackages,
                NotAssignedPackages = notAssignedPackages.OrderBy(p => p)
            };
            return View(vm);
        }

    }
}